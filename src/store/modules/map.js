export default {
  namespaced: true,
  state: {
    schoolForm: null // 存储学校表单数据
  },

  getters: {

    getSchoolForm (state) {
      return state.schoolForm
    }

  },

  mutations: {

    updateSchoolForm (state, data) {
      state.schoolForm = data
    }

  }
}
